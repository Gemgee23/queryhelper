import React from 'react';

const ContractData = [
    {
        "fieldName": "Agreement ID",
        "description": "Unique non-intelligent key which defines a contract/effective date.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Effective Date",
        "description": "Unique non-intelligent key which defines a contract/effective date.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Expiration Date",
        "description": "Unique non-intelligent key which defines a contract/effective date.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Policy Symbol Code",
        "description": "Policy Symbol Code is shorthand representation used to uniquely describe the product classification of the policy.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Market Code",
        "description": "Market Code is a shorthand representation used to describe the high-level sales and underwriting unit responsible for an account as a whole.  Historically it has been the 4th position of the Contract Number for CM formatted policies.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Underwriting Subdivision Code",
        "description": "Underwriting Subdivision Code is a shorthand representation used to uniquely describe the specific segment of business within a given Liberty Mutual division. Historically this has been the 6th position of the Contract Number for CM formatted policies.  Sometimes known as Policy Territory Code.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Stakeholder Identifier",
        "description": "Stakeholder Identifier is a non-meaningful Liberty Mutual Group assigned number used internally as a system key to uniquely identify an entity or individual.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Special Designator Code",
        "description": "Special Designator Code is a shorthand representation of a Liberty Mutual assigned value that reflects the coverage type as it relates to a high-level industry segment for a specific line of business on a contract.  This field is used in conjunction with the Type Risk Code",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Franchise Association Code",
        "description": "Franchise Association Code is a shorthand representation used to uniquely describe a specific association, franchise, or other grouping to which a contract is assigned.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Contract Effective Date",
        "description": "Contract Effective Date is the date on which the contract becomes legally binding.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    }

]
export default ContractData;