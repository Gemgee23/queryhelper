import React from 'react';

const ClearQueryButton = (props) => (

    <div>
        <button type="button" onClick={props.clearString} class="btn btn-primary btn-lg" id="clearQuery">Clear Query</button>
    </div>
)
export default ClearQueryButton;