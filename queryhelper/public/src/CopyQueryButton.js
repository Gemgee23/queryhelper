import React, { Fragment } from 'react';

const CopyQueryButton = (props) => (
    <Fragment>
        <div>
            <button type="button" class="btn btn-primary btn-lg" id="copyQuery">Copy Query</button>
        </div>
    </Fragment>)
export default CopyQueryButton;