import React from 'react';
import Select from 'react-select';

const subjectAreaOptions = [
    { label: "Premium", value: 0 },
    { label: "Contract", value: 1 },
];

const SubjectArea = (props) => (
    <div className="container">
        <div className="row">
            <div className="col-md-4"></div>
            <div className="col-md-4">
                <Select options={subjectAreaOptions} selectSubjectArea={subjectAreaOptions.value} onChange={props.selectSubjectArea}
                />
            </div>
            <div className="col-md-4"></div>
        </div>
    </div>
);
export default SubjectArea;