import React from 'react';

const PremiumData = [
    {
        "fieldName": "Estimated State Standard Premium Amount",
        "description": "Estimated State Standard Premium Amount is the monetary value of premium an insured would pay in a particular state based on the state premium algorithm prior to the application of any discounts or premium modification.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Manual Premium Amount",
        "description": "Manual Premium Amount is the monetary value of the manual premium from the exposure (including any audits) for the job classes.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Deductible Identifier",
        "description": "Deductible Identifier is a non-meaningful Liberty Mutual Group assigned number used to uniquely identify the occurrence of a specific deductible applicable to the coverage.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Financial Management Line of Business Code",
        "description": "Financial Management Line of Business Code is a shorthand representation used to uniquely describe the proprietary Liberty Mutual codes expressing the major lines of business within financial reporting.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Standard Premium Amount",
        "description": "Standard Premium Amount is the monetary value of experience modified premium after application of some deductible credits but prior to retro and premium discounts, including any audits.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    },
    {
        "fieldName": "Rate Deviation Factor",
        "description": "Rate Deviation Factor is a numeric value used to reflect the difference between the Bureau Rates and the filed rates used in pricing a policy. These factors are filed with the Departments of Insurance in states where such deviations are allowed.",
        "link": <a href="https://libertymutual.sharepoint.com/teams/DataGovernance/DSD/SitePages/Home.aspx">Link to DD</a>,
    }

]

export default PremiumData;