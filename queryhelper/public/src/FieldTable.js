import React, { Fragment } from 'react';
import Checkbox from './Checkbox';

const FieldTable = (props) => (
    <Fragment> <table>
        <tbody>
            <tr>
                <th>Select Field</th>
                <th>Field Name</th>
                <th>Description</th>
                <th>Link</th>
            </tr>
            {props.tableData && props.tableData.map((rowData, index) => (
                <tr>
                    <td><Checkbox key={rowData[0]} fieldName={rowData[0]} index={index} updateQuery={(tableRowIndex) => props.updateQuery(tableRowIndex)} /></td>
                    {Object.values(rowData).map((cellData) => (
                        <td>{cellData}</td>
                    ))}
                </tr>
            ))}
        </tbody>
    </table>
    </Fragment >)
export default FieldTable;

