import Header from './Header';
import React, { Fragment } from 'react';
import SubjectArea from './SubjectArea';
import FieldTable from './FieldTable';
import QueryDisplayBox from './QueryDisplayBox';
import GenerateQueryButton from './GenerateQueryButton';
import ClearQueryButton from './ClearQueryButton';
import CopyQueryButton from './CopyQueryButton';
import ImportantInfoBox from './ImportantInfoBox';
import PremiumData from './PremiumData';
import ContractData from './ContractData';

const tableArray = ["premium_t", "contract_t"]

var subjectAreaValue;

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            queryString: "",
            tableData: [{
                "fieldName": "",
                "description": "",
                "link": <a href=""></a>,
            }],
            queryArray: [],
            subjectAreaIndex: 0,
            subjectAreaArray: [0, 1],
            copied: false,
        }
        this.fromTableArray = []

    }
    selectSubjectArea = (subjectArea) => {
        subjectAreaValue = subjectArea.value;
        if (subjectAreaValue === 0) {
            this.setState({ subjectAreaIndex: 0 })
            this.setState({ tableData: PremiumData })
        } else {
            this.setState({ subjectAreaIndex: 1 })
            this.setState({ tableData: ContractData })
        }
        if (!this.fromTableArray.includes(subjectAreaValue)) {
            this.fromTableArray.push(subjectAreaValue)
        }
    }

    updateQuery = (tableRowIndex) => {

        let tempArray = this.state.queryArray
        if (!this.state.queryArray.includes(this.state.tableData[tableRowIndex].fieldName)) {
            tempArray.push(this.state.tableData[tableRowIndex].fieldName)
        } else {
            const tempIndex = tempArray.indexOf(this.state.tableData[tableRowIndex].fieldName)
            tempArray.splice(tempIndex)
        }

        this.setState({ queryArray: tempArray })

    }
    createSQLString = () => {
        let queryArrayLength = this.state.queryArray.length;
        let querySelectString = "Select "
        let resultString = "";
        let tableString = ""
        for (let k = 0; k < this.fromTableArray.length; k++) {
            if (k < this.fromTableArray.length - 1) {
                tableString += ((tableArray[k]) + ", ")
            } else {
                tableString += ((tableArray[k]))
            }
        }
        //let from = " FROM " + tableString + ";"
        var from = " FROM CI_NI_ALYTC_AE_T_D.CCO_CI_Combined_Cntr_Hist_V cco_cc "
            + " CROSS JOIN "
            + " CI_NI_ALYTC_AE_T_D.PCC_As_Of_Date_v val "
            + " LEFT OUTER JOIN "
            + " CI_NI_ALYTC_AE_T_D.PCC_WC_Classification_Cov_V pcc_wccc "
            + " ON cco_cc.Agreement_ID = pcc_wccc.Agreement_ID "
            + " AND pcc_wccc.Row_Effective_Dte <=  (val.As_Of_Dte + 1) "
            + " AND pcc_wccc.Row_Expiration_Dte > (val.As_Of_Dte + 1) "
            + " WHERE "
            + " CAST(cco_cc.ADW_Row_Effective_Datetime AS DATE) <= (val.As_Of_Dte + 1) "
            + " AND CAST(cco_cc.ADW_Row_Expiration_Datetime AS DATE) > (val.As_Of_Dte + 1) "
            + " SAMPLE 1000;"

        for (var i = 0; i < queryArrayLength; i++) {
            resultString += ((this.state.queryArray[i]) + ", ");
        }
        resultString = resultString.replace(/,(?=[^,]*$)/, '')
        querySelectString += resultString += from;
        this.setState({ queryString: querySelectString })
    }

    clearQuery = () => {
        var clearString = "";
        this.setState({ queryString: clearString })
        this.setState({ tableData: [] })
        this.setState({ queryArray: [] })
        this.setState({ subjectAreaIndex: 0 })
    }

    onCopy = () => {

        this.setState({ copied: true })
    }
    render = () => (
        <Fragment>
            <div id="header">
                <Header
                />
            </div>
            <ImportantInfoBox
            />
            <div id="right-side-content">
                <div class="row">
                    <div class="col-sm-3" id="table-dropdown-content-area">
                        <div class="dropdown" id="subjectDropdown">

                            <SubjectArea
                                selectSubjectArea={this.selectSubjectArea}
                            />
                        </div>
                    </div>

                    <div id="querySelector" class="col-sm-12">
                        {this.state.subjectAreaArray.map((subjectAreaIndex) => {
                            return (this.state.subjectAreaIndex === subjectAreaIndex && (
                                <FieldTable
                                    key={subjectAreaIndex}
                                    tableData={this.state.tableData}
                                    updateQuery={(tableRowIndex) => this.updateQuery(tableRowIndex)}
                                />
                            ))
                        })
                        }
                    </div>
                </div>
                <br>
                </br>
                <div id="button-textfield-content-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                                <GenerateQueryButton
                                    createSQLString={this.createSQLString}
                                />
                            </div>
                            <div class="col-sm-4">
                                <ClearQueryButton
                                    clearQuery={this.clearQuery}
                                />
                            </div>
                            <div class="col-sm-4">
                                <CopyQueryButton
                                />
                            </div>
                        </div>
                    </div>

                    <div id="queryText">
                        <h3>
                            Generated Query:
                        </h3>
                        <QueryDisplayBox
                            queryString={this.state.queryString}
                        />
                    </div>
                </div>
            </div>

        </Fragment>
    )
}
export default App;
