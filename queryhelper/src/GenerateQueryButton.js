import React from 'react';

const GenerateQueryButton = (props) => (

    <div>
        <button type="button" onClick={props.createSQLString} class="btn btn-primary btn-lg" id="generateQuery" >Generate Query</button>
    </div>

)
export default GenerateQueryButton;
