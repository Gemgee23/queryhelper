import Header from './Header';
import React, { Fragment } from 'react';
import SubjectArea from './SubjectArea';
import FieldTable from './FieldTable';
import QueryDisplayBox from './QueryDisplayBox';
import GenerateQueryButton from './GenerateQueryButton';
import ClearQueryButton from './ClearQueryButton';
import CopyQueryButton from './CopyQueryButton';
import ImportantInfoBox from './ImportantInfoBox';
import PremiumData from './PremiumData';
import ContractData from './ContractData';

//const queryArray = []

var subjectAreaValue;

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            queryString: "",
            tableData: [],
            queryArray: [],
            subjectAreaIndex: [0, 1],
        }

    }
    //Put functions here

    selectSubjectArea = (subjectArea) => {
        subjectAreaValue = subjectArea.value;
        if (subjectAreaValue === 0) {
            this.setState({ subjectAreaIndex: 0 })
            this.setState({ tableData: PremiumData })
        } else {
            this.setState({ subjectAreaIndex: 1 })
            this.setState({ tableData: ContractData })
        }
    }

    updateQuery = (tableRowIndex) => {

        let tempArray = this.state.queryArray
        if (!this.state.queryArray.includes(this.state.tableData[tableRowIndex].fieldName)) {
            tempArray.push(this.state.tableData[tableRowIndex].fieldName)
            console.log("not found")
        } else {

            const tempIndex = tempArray.indexOf(this.state.tableData[tableRowIndex].fieldName)
            tempArray.splice(tempIndex)

            console.log("found")
            console.log(tableRowIndex)
            console.log("TEMPARRAY", tempArray)
        }

        this.setState({ queryArray: tempArray })

    }
    createSQLString = () => {
        var queryArrayLength = this.state.queryArray.length;
        var querySelectString = "Select "
        var resultString = "";
        var from = " FROM premium_t;"

        for (var i = 0; i < queryArrayLength; i++) {
            resultString += ((this.state.queryArray[i]) + ", ");
        }
        resultString = resultString.replace(/,(?=[^,]*$)/, '')
        querySelectString += resultString += from;
        this.setState({ queryString: querySelectString })
    }

    clearQuery = () => {
        var clearString = "";
        this.setState({ clearString: clearString })
    }
    render = () => (
        <Fragment>
            <div>

                <Header
                />
                <ImportantInfoBox
                />

                <div id="table-dropdown-content-area">
                    <div class="dropdown" id="subjectDropdown">

                        <SubjectArea
                            selectSubjectArea={this.selectSubjectArea}
                        />
                    </div>
                </div>
                <br></br>
                <FieldTable
                    tableData={this.state.tableData}
                    updateQuery={(tableRowIndex) => this.updateQuery(tableRowIndex)}
                />



                <br>
                </br>
                <div>
                    <div id="buttons">
                        <GenerateQueryButton
                            createSQLString={this.createSQLString}
                        />
                        <ClearQueryButton
                            clearString={this.clearString}
                        />
                        <CopyQueryButton
                        />
                    </div>
                    <div id="queryBox">
                        <p>
                            Generated Query:
                        </p>

                        <QueryDisplayBox
                            queryString={this.state.queryString}
                        />
                    </div></div>

            </div>
        </Fragment>
    )
}
export default App;
