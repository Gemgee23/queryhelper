import React from 'react';

const Checkbox = (props) => (
    <div>
        <input key={props.fieldName} onClick={() => props.updateQuery(props.index)} type="checkbox" />
    </div>
)
export default Checkbox;