import React, { Fragment } from 'react';
//import { CopyToClipboard } from 'react-copy-to-clipboard';

const CopyQueryButton = (props) => (
    <Fragment>
        <div>
            <button type="button" onClick={props.onCopy} class="btn btn-primary btn-lg" id="copyQuery">Copy Query</button>
        </div>
    </Fragment>)
export default CopyQueryButton;