import React from 'react';
import Select from 'react-select';

const subjectAreaOptions = [
    { label: "Premium", value: 0 },
    { label: "Contract", value: 1 },
];

const SubjectArea = (props) => (
    <div className="container">
        <div className="row">

            <div style={{ width: '200px' }}>
                <Select options={subjectAreaOptions} selectSubjectArea={subjectAreaOptions.value} onChange={props.selectSubjectArea} autosize={true}
                    defaultValue={{ label: "Select Subject Area", value: 5 }} />
            </div>

        </div>
    </div>
);
export default SubjectArea;