import React from 'react';
import './App.css';

function Header() {
  return (
    <header className="App-header">
      <h2 id="header">
        Welcome to the Query Helper
      </h2><span class="glyphicon glyphicon-search" ></span>
    </header>

  );
}

export default Header;
